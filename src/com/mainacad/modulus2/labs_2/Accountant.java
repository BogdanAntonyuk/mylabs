package com.mainacad.modulus2.labs_2;

import java.util.Scanner;

public class Accountant {
    public static void main(String[] args) {
        Employee employee = new Employee();
        Scanner input = new Scanner(System.in);
        double[] salary = new double[3];
        System.out.print("Type in employees name: ");
        String name = input.nextLine();
        System.out.println("Enter every salary for a given period (press enter after each instance of salary): ");
        for (int i = 0;i<salary.length;i++){
            salary[i] = input.nextDouble();
        }

        employee.calcSalary(name,salary);
    }
}
