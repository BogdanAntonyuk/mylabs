package com.mainacad.modulus2.labs_2;

public class Matrix {

    public static void main(String[] args) {
        Matrix matrix = new Matrix();
        int[][] inputMatrix1 = {{1,2,4,8},{4,3,4,3},{8,5,4,3}};
        int[][] inputMatrix2 = {{1,2,4,3},{4,3,4,3},{5,3,2,4}};
        matrix.sumMatrix(inputMatrix1,inputMatrix2);

    }
    void sumMatrix (int inputMatrix1[][],int inputMatrix2[][]){
        int[][] outputMatrix = new int[inputMatrix1.length][inputMatrix2.length];
        for (int i=0;i<outputMatrix.length;i++){
            for (int j=0;j<outputMatrix[i].length;j++){
                outputMatrix[i][j] = inputMatrix1[i][j] + inputMatrix2[i][j];
            }
        }
        printArray(outputMatrix);




    }
    void printArray(int[][] outputMatrix){
        for (int i = 0; i<outputMatrix.length;i++){
            for (int j=0;j<outputMatrix[i].length;j++){
                if (outputMatrix[i][j]<10||outputMatrix[i][j]<-10){
                    System.out.print(" "+outputMatrix[i][j]+" ");
                }else System.out.print(outputMatrix[i][j]+" ");
            }
            System.out.println();
        }

    }



}
