package com.mainacad.modulus2.labs_2;

import org.w3c.dom.ls.LSOutput;

public class Employee {

    static void calcSalary(String name, double... salary){
        double totalSalary=0;
        for (double v : salary) {
            totalSalary += v;
        }
        System.out.println("The Employee name is: "+name+ "\nEmployee total salary is: "+totalSalary);
    }
}
