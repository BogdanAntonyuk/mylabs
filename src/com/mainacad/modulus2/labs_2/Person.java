package com.mainacad.modulus2.labs_2;

public class Person {
    String firstName;
    String lastName;
    int age;
    String gender;
    int phoneNumber;

    public static void main(String[] args) {
        personalInfo("Bogdan", "ANt",32,"male");
    }
    static void personalInfo(String firstName, String lastName, int age, String gender, int phoneNumber){
        System.out.println("First Name: "+firstName+"\nLast Name: "+lastName+"\nAge: "+age+"\nGender: "+gender+"\nPhone Number: "+phoneNumber);
    }
    static void personalInfo(String firstName, int age, String gender){
        System.out.println("First Name: "+firstName+"\nAge: "+age+"\nGender: "+gender);
    }
    static void personalInfo(String firstName, String lastName, String gender){
        System.out.println("First Name: "+firstName+"\nLast Name: "+lastName+"\nGender: "+gender);
    }
    static void personalInfo(String firstName, String lastName, int age, String gender){
        System.out.println("First Name: "+firstName+"\nLast Name: "+lastName+"\nAge: "+age+"\nGender: "+gender);
    }
    static void personalInfo(String firstName){
        System.out.println("First Name: "+firstName);
    }

}
