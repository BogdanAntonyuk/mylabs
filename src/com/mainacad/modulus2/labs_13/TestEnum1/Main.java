package com.mainacad.modulus2.labs_13.TestEnum1;

import java.lang.reflect.Array;
import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {
    enum MyDaysOfWeek{
        SUNDAY,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY;
        public MyDaysOfWeek nexDay(){
            MyDaysOfWeek[] days = MyDaysOfWeek.values();
            int index = ordinal();
            if (index == days.length-1){
                index = 0;
            }
            else index++;
            return days[index];
        }
    }
    public static void main(String[] args) {
        for (MyDaysOfWeek days: MyDaysOfWeek.values()) {
            switch (days){
                case TUESDAY: case THURSDAY:
                    System.out.println("My java day: "+days);
                    break;

            }

        }
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the day of the week: ");
        String day = input.next();

        try {
            MyDaysOfWeek daysOfWeek = MyDaysOfWeek.valueOf(day.toUpperCase());
            System.out.println(daysOfWeek.nexDay());
        }
        catch (IllegalArgumentException e){
            System.out.println("Incorrect day of week");

        }



    }
}
