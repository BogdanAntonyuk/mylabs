package com.mainacad.modulus2.labs_13.TestEnum2;

enum Suit{
    SPACE,
    DIAMOND,
    CLUB,
    HEART;
}
enum Rank{
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    JACK,
    QUEEN,
    ACE,
    KING;
}
class Cart{
    private Suit cardSuite;
    private Rank cardRank;
Cart(Suit cardSuite, Rank cardRank){
this.cardSuite = cardSuite;
this.cardRank = cardRank;
}

    @Override
    public String toString() {
        return "Cart{" +
                "cardSuite = " + cardSuite +
                ", cardRank = " + cardRank +
                '}';
    }
}

public class Main {

    public static void main(String[] args) {
        Cart[] carts = new Cart[52];

        int index = 0;
        for (Suit suit:Suit.values())
            for (Rank rank:Rank.values()) {
                carts[index++] = new Cart( suit,rank);
            }
        for (Cart cartsInDeck: carts) {
            System.out.println(cartsInDeck);
        }
    }
}

