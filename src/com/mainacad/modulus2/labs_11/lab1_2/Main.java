package com.mainacad.modulus2.labs_11.lab1_2;

public class Main {
    public static void main(String[] args) {
        try {
            throw new Exception("Exception");
//            System.out.println("Work?");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("I was there");
        }
    }
}

class MyExeption extends Exception{
    String message;
    MyExeption(String message){
        this.message = message;

    }
    public void printMessage(){
        System.out.println(message);

    }
}

class MainMyExeption{
    public static void main(String[] args) {
        try {
            throw new MyExeption("This is my EXEPTION!!!");
        }
        catch (MyExeption e){
            e.printMessage();
        }
        MyTest myTest = null;
        try {
            myTest.test();

        }
        catch (MyExeption | NullPointerException e){
//            e.printMessage();
            System.out.println("cool");
        }





//        MyTest myTest = new MyTest();
//
//        try {
//            myTest.test();
//
//        }
//        catch (MyExeption e){
//            System.out.println("done");
//        }
    }
}

class MyTest{
    public void test() throws MyExeption{
        throw new MyExeption("Test!");
    }
}
