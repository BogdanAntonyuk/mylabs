package com.mainacad.modulus2.labs_11.lab3;

public class Person {
    String firstName, lastName;
    int age;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age >0 && age <121){
        this.age = age;}
        else throw new InvalidAgeExeption("Age is out of bounds (0-120)");
    }




}

class InvalidAgeExeption extends RuntimeException{
    InvalidAgeExeption(String message){
        super(message);
    }
}
class Main{
    public static void main(String[] args) {
        Person person = new Person();
        person.setAge(140);
        System.out.println("Your age is: "+person.age);
    }
}
