package com.mainacad.modulus2.labs_10.testwraper;

public class Main {
    public static void main(String[] args) {
        Integer x1 = 10;
        Integer x2 = new Integer(10);
        Integer x3 = Integer.valueOf(10);
        Integer x4 = Integer.valueOf("10");
        Integer x5 = Integer.parseInt("10");
        System.out.println("1 - "+(x1==x2));
        System.out.println("1 - "+ x1.equals(x2));
        System.out.println("2 - "+(x3==x4));
        System.out.println("2 - "+x3.equals(x4));
        System.out.println("3 - "+(x4==x5));
        System.out.println("3 - "+x4.equals(x5));
        System.out.println("4 - "+(x5==x2));
        System.out.println("4 - "+x2.equals(x5));





         x1 = 1000;
         x2 = new Integer(1000);
         x3 = Integer.valueOf(1000);
         x4 = Integer.valueOf("1000");
         x5 = Integer.parseInt("1000");
        System.out.println("1 - "+(x1==x2));
        System.out.println("1 - "+ x1.equals(x2));
        System.out.println("2 - "+(x3==x4));
        System.out.println("2 - "+x3.equals(x4));
        System.out.println("3 - "+(x4==x5));
        System.out.println("3 - "+x4.equals(x5));
        System.out.println("4 - "+(x5==x2));
        System.out.println("4 - "+x2.equals(x5));



    }
}
