package com.mainacad.modulus2.labs_10.lab_3_4_5;

import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;


interface Drawable{
    public void draw();
}

class Main{
    public static void main(String[] args) {
        System.out.println("Input number of shape");
        Scanner scanner = new Scanner(System.in);
        int numOfShapes = scanner.nextInt();

        Shape[] arrOfShapes = new Shape[numOfShapes];

        System.out.print("Enter shape on new line (q - exit)");

        while (true){
            String input = scanner.nextLine();
            if (input.equals("q")){break;}

            if (Shape.parseShape(input)!=null){
                for (int i = 0; i < arrOfShapes.length; i++){
                    if (arrOfShapes[i]==null){
                        arrOfShapes[i]=Shape.parseShape(input);
                        break;
                    }
                }
            }
        }
        scanner.close();

        for (Shape shape:arrOfShapes){
            shape.draw();
        }
    }

}

public abstract class Shape implements Drawable {
    protected double  area;
    protected String shape;
    protected String color;
    public abstract double calcArea();

    public static Shape parseShape(String input){
        String[] string = input.split(":|,");

        String typeOfShape = string[0].toLowerCase();

        Shape shape = null;

        switch (typeOfShape){
            case "rectangle":
                shape = new Rectangle(string[1],Double.parseDouble(string[2]),Double.parseDouble(string[3]));
                return shape;

            case "triangle":
                shape = new Triangle(string[1],Double.parseDouble(string[2]),Double.parseDouble(string[3]),Double.parseDouble(string[4]));
                return shape;

            case "circle":
                shape = new Circle(string[1],Double.parseDouble(string[2]));
                return shape;

            default:
                System.out.println("Ellegal input");
                return null;
        }
    }


    @Override
    public void draw() {
        System.out.println(toString());

    }




    @Override
    public String toString() {
        return "Shape{" +
                "area=" + area +
                ", shape='" + shape + '\'' +
                ", color='" + color + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Shape)) return false;
        Shape shape1 = (Shape) o;
        return Double.compare(shape1.area, area) == 0 &&
                Objects.equals(shape, shape1.shape) &&
                Objects.equals(color, shape1.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(area, shape, color);
    }
}


class Circle extends Shape implements Comparable{

    @Override
    public int compareTo(Object o) {
        Shape circle = (Shape)o;
        if(this.area > circle.area) return 1;
        if(this.area<circle.area) return -1;
        return 0;
    }


    private double radius;
    final double PI = 3.14;

    @Override
    public double calcArea() {
        area = PI*(radius*radius);
        return area;
    }

    Circle(String color, double radius){
        this.color = color;
        this.radius = radius;
        shape = "Circle";
        calcArea();

    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", color ="+ color+", Shape = "+shape+

                ", area = "+area+
                '}';

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Circle)) return false;
        if (!super.equals(o)) return false;
        Circle circle = (Circle) o;
        return Double.compare(circle.radius, radius) == 0 &&
                Double.compare(circle.PI, PI) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), radius, PI);
    }


}


class Rectangle extends Shape implements Comparable{
    private double width;
    private double height;


    @Override
    public int compareTo(Object o) {
        Shape rectangle = (Shape)o;
        if(this.area > rectangle.area) return 1;
        if(this.area < rectangle.area) return -1;
        return 0;
    }



    @Override
    public double calcArea(){
        area = width*height;
        return area;
    }

    Rectangle(String color, double width, double height){
        this.color = color;
        this.width = width;
        this.height = height;
        calcArea();
        shape = "Rectangle";

    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "color='" + color + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", area=" + area +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rectangle)) return false;
        if (!super.equals(o)) return false;
        Rectangle rectangle = (Rectangle) o;
        return Double.compare(rectangle.width, width) == 0 &&
                Double.compare(rectangle.height, height) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), width, height);
    }
}


class Triangle extends Shape implements Comparable{
    private double a,b,c;


    @Override
    public int compareTo(Object o) {
        Shape triangle = (Shape)o;
        if(this.area>triangle.area) return 1;
        if(this.area<triangle.area) return -1;
        return 0;
    }


    @Override
    public double calcArea(){
        double s = (a+b+c)/2;
        area = Math.sqrt(s*(s-a)*(s-b)*(s-c));
        return area;
    }


    Triangle(String color, double a,double b,double c){
        this.color = color;
        shape = "Triangle";
        this.a = a;
        this.b = b;
        this.c = c;
        calcArea();

    }


    @Override
    public String toString() {
        return "Triangle{" +
                "shape='" + shape + '\'' +
                ", color='" + color + '\'' +
                ", a=" + a +
                ", b=" + b +
                ", c=" + c + ", area=" + area+
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Triangle)) return false;
        if (!super.equals(o)) return false;
        Triangle triangle = (Triangle) o;
        return Double.compare(triangle.a, a) == 0 &&
                Double.compare(triangle.b, b) == 0 &&
                Double.compare(triangle.c, c) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), a, b, c);
    }
}

class mainRec{
    Shape rec1 = new Rectangle("bl",5,5);
    Shape rec2 = new Rectangle("re",3,5);

    public static void main(String[] args) {
        System.out.println();
    }
}


class Main1 {
    public static void main(String[] args) {

        Drawable[] arr = {
                new Circle("R", 3),
                new Circle("H", 6),
                new Rectangle("J",5,5),
                new Rectangle("M",6,5),
                new Triangle("L",5,4,3),
                new Triangle("K", 2,2,2)
        };
        Arrays.sort(arr);


        for(Drawable ob : arr)
            ob.draw();




    }
}
