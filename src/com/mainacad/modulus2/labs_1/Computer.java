package com.mainacad.modulus2.labs_1;

import java.util.Arrays;

public class Computer {
    private String manufacturer;
    private int serialNumber;
    private float price;
    private int quantityCPU;
    private double frequencyCPU;
    public Computer() {};
    public void setManufacturer(String manufacturer){
        this.manufacturer = manufacturer;
    }
    public String  getManufacturer(){
        return manufacturer;
    }
    public void setSerialNumber(int serialNumber){
        this.serialNumber = serialNumber;
    }
    public int getSerialNumber(){
        return serialNumber;
    }
    public void setPrice(float price){
        this.price = price;
    }
    public float getPrice(){
        return price;
    }
    public void setQuantityCPU(int quantityCPU){
        this.quantityCPU = quantityCPU;
    }
    public int getQuantityCPU(){
        return quantityCPU;
    }
    public void setFrequencyCPU (double frequencyCPU){
        this.frequencyCPU = frequencyCPU;
    }
    public double getFrequencyCPU(){
        return frequencyCPU;
    }

    public String toString(){
        return "Computer {" + "Manufacturer = " + manufacturer +
                ", Serial number =" + serialNumber +
                ", Price=" + String.format("%.2f", price) +
                ", Quantity CPU =" + quantityCPU +
                ", Frequency CPU =" + String.format("%.2f",frequencyCPU) + '}' + "\n";
    }

    public void view(String numberOfComp){
        System.out.println(numberOfComp+"Manufacturer = " + manufacturer +
                ", Serial number =" + serialNumber +
                ", Price=" + String.format("%.2f", price) +
                ", Quantity CPU =" + quantityCPU +
                ", Frequency CPU =" + String.format("%.2f",frequencyCPU));
    }


}
