package com.mainacad.modulus2.labs_7;

import java.util.Objects;

public class Device {
    String manufacturer;
    float price;
    String serialNumber;

    public String getManufacturer() {
        return manufacturer;
    }

    public float getPrice() {
        return price;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}

class Monitor extends Device{
    int resolutionX;
    int resolutionY;

    public int getResolutionX() {
        return resolutionX;
    }

    public int getResolutionY() {
        return resolutionY;
    }

    public void setResolutionX(int resolutionX) {
        this.resolutionX = resolutionX;
    }

    public void setResolutionY(int resolutionY) {
        this.resolutionY = resolutionY;
    }

    @Override
    public String toString() {
        return "Monitor{" +
                "resolutionX=" + resolutionX +
                ", resolutionY=" + resolutionY +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Monitor)) return false;
        Monitor monitor = (Monitor) o;
        return resolutionX == monitor.resolutionX &&
                resolutionY == monitor.resolutionY;
    }

    @Override
    public int hashCode() {
        return Objects.hash(resolutionX, resolutionY);
    }
}

class EthernetAdapter extends Device{
    private int speed;
    private String mac;

    public String getMac() {
        return mac;
    }
    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    @Override
    public String toString() {
        return "ethernetAdapter{" +
                "speed=" + speed +
                ", mac='" + mac + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EthernetAdapter)) return false;
        EthernetAdapter that = (EthernetAdapter) o;
        return speed == that.speed &&
                Objects.equals(mac, that.mac);
    }

    @Override
    public int hashCode() {
        return Objects.hash(speed, mac);
    }
}

class Main{
    public static void main(String[] args) {
        Device[] devices = new Device[2];
        Monitor monitor = new Monitor();
        monitor.resolutionX = 1024;
        monitor.resolutionY = 576;
        monitor.setPrice(356.44f);
        monitor.setManufacturer("Samsung");
        monitor.setSerialNumber("Media1A");

        EthernetAdapter ethernetAdapter = new EthernetAdapter();
        ethernetAdapter.setSpeed(1000);
        ethernetAdapter.setMac("213 87324 987342");
        ethernetAdapter.setPrice(14.99f);
        ethernetAdapter.setManufacturer("Huawei");
        ethernetAdapter.setSerialNumber("F430A");

        devices[0] = monitor;
        devices[1] = ethernetAdapter;

        for (Device device:devices){
            System.out.println(device.toString());
        }

    }
}