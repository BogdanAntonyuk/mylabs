package com.mainacad.modulus2.labs_9.testSplitter;

public class Main {
    public static void main(String[] args) {
        String myStr = "This is String, split by StringSplitter. Created by Student: Bogdan";
        System.out.println("Split by \"comma\":");
        for (String retval : myStr.split(",")){
            System.out.println(retval);
        }
        System.out.println("\n\nSplit by \"space\":");
        for (String retval : myStr.split(" ")){
            System.out.println(retval);
        }
        System.out.println("\n\nSplit by \"dot\":");
        for (String retval : myStr.split("\\.")){
            System.out.println(retval);
        }

    }
}
