package com.mainacad.modulus2.labs_9.testStrings1;

public class main {
    public static void main(String[] args) {
        String myStr = "abracadabra";
        System.out.println("The index of first \"ra\" in String is: " + myStr.indexOf("ra"));
        System.out.println("The index of last \"ra\" in String is: " + myStr.lastIndexOf("ra"));
        System.out.println(myStr.subSequence(3,7));
        System.out.println(reverseString(myStr));




    }
    static String reverseString(String myString){
        char[] arr = myString.toCharArray();
        int n = arr.length;
        char[] b = new char[n];
        int j = n;
        for (int i =0; i<n; i++){
            b[j-1] = arr[i];
            j = j-1;
        }
        String a = String.copyValueOf(b);
//        String a  = new String(b);
        return a;


    }
}
