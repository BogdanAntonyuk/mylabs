package com.mainacad.modulus2.labs_12.labs1_2;

import java.util.Arrays;
import java.util.Comparator;

public class MyPhone {

    MyPhone(){
        int phonebook;

    }






    class MyPhoneBook {

        class PhoneNumber{

            @Override
            public String toString() {
                return "PhoneNumber{" +
                        "name='" + name + '\'' +
                        ", phone='" + phone + '\'' +
                        '}';
            }

            private String name, phone;

            PhoneNumber(String name, String phone){
                this.name = name;
                this.phone = phone;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }
        }


        private MyPhoneBook.PhoneNumber[] phoneNumbers = new MyPhoneBook.PhoneNumber[10];


        public void addPhoneNumber(String name, String phone){
            for(int i=0; i<phoneNumbers.length; i++){
                if(phoneNumbers[i] ==null){
                    phoneNumbers [i] = new MyPhoneBook.PhoneNumber(name, phone);
                    break;
                }
            }
        }
        void printPhoneBook(){
            for (int i = 0; i<phoneNumbers.length; i++){
                if(phoneNumbers[i] !=null){
                    System.out.println(phoneNumbers[i].toString());

                }
                else break;
            }
        }

        public void sortByName(){
            Arrays.sort(phoneNumbers, new Comparator<PhoneNumber>() {
                @Override
                public int compare(MyPhoneBook.PhoneNumber o1, MyPhoneBook.PhoneNumber o2) {
                    if (o1!=null&&o2!=null) {
                        return o1.name.compareToIgnoreCase(o2.name);
                    }
                    else return 0;
                }

            });

        }

        public void sortByNumber(){
            Arrays.sort(phoneNumbers, new Comparator<PhoneNumber>() {
                @Override
                public int compare(MyPhoneBook.PhoneNumber o1, MyPhoneBook.PhoneNumber o2) {
                    if (o1!=null&&o2!=null) {
                        return o1.phone.compareToIgnoreCase(o2.phone);
                    }
                    else return 0;
                }

            });

        }
    }
}


class Main{
    public static void main(String[] args) {
        MyPhone myPhone = new MyPhone();
        MyPhone.MyPhoneBook myPhoneBook = myPhone.new MyPhoneBook();
        myPhoneBook.addPhoneNumber("Sasha", "050124356");
        myPhoneBook.addPhoneNumber("Vova", "0934576893");
        myPhoneBook.addPhoneNumber("Alexa", "09442231");
        myPhoneBook.addPhoneNumber("LEX", "0509838727843");
        myPhoneBook.addPhoneNumber("Alexa", "093435454");
        myPhoneBook.printPhoneBook();
        myPhoneBook.sortByName();
        System.out.println();
        System.out.println("Sorted by name");
        myPhoneBook.printPhoneBook();
        System.out.println();
        System.out.println("Sorted by number");
        myPhoneBook.sortByNumber();
        myPhoneBook.printPhoneBook();
    }
}
