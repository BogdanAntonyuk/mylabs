package com.mainacad.modulus2.labs_14.TestGenerics2;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Random;

public class MyTestMethod {
    static int index;
    static <T> void calcNum(T [] array, T maxElem){

        for (int i = 0;i<array.length;i++){
            if((Double) array[i] >(int) maxElem){
                index++;
            }
        }
        System.out.println("Array values: "+ Arrays.toString(array)+
                "\nNumber of elements that are greater than "+ maxElem +": "+index);

    }
}
class Main{
    public static void main(String[] args) {
        Integer[] arrOfInt = new Integer[10];
        Double[] arrOfDoubles = new Double[10];
        Random r = new Random();

        for (int i=0;i<arrOfInt.length;i++){
            arrOfInt[i] = r.nextInt(11);
        }
        for (int i = 0;i<arrOfDoubles.length;i++){
            arrOfDoubles[i] = Math.random()*10;
            arrOfDoubles[i] = Math.round(arrOfDoubles[i]*100.0)/100.0;

        }
        MyTestMethod.calcNum(arrOfDoubles,3);


        System.out.println("\n");
        System.out.println(Arrays.toString(arrOfDoubles));



    }
}
