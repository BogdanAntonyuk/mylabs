package com.mainacad.modulus2.labs_14.TestGenerics1;

import org.w3c.dom.ls.LSOutput;

public class MyTuple<A,B,C> {

    private A a;
    private B b;
    private C c;

    public A getA() {
        return a;
    }

    public void setA(A a) {
        this.a = a;
    }

    public B getB() {
        return b;
    }

    public void setB(B b) {
        this.b = b;
    }

    public C getC() {
        return c;
    }

    public void setC(C c) {
        this.c = c;
    }

    @Override
    public String toString() {
        return "MyTuple{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }



    MyTuple (A a, B b, C c){
        this.a = a;
        this.b = b;
        this.c = c;





    }





}

class Main {

    public static void main(String[] args) {
        MyTuple myTuple1 = new MyTuple("bob", Integer.valueOf(10), Long.valueOf(10L));
        MyTuple<? extends Number, String, String> myTuple2 = new MyTuple<Double, String, String>(Double.valueOf(18.3), "ko", "MO");
        System.out.println(myTuple1.toString());
        System.out.println(myTuple2.toString());

    }




}
