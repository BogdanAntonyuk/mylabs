package com.mainacad.modulus2.labs_15.lab1_6.testCollections3;

import java.util.*;

public class Main3 {
    public static void main(String[] args) {
        List <String> arrList = new ArrayList<>();
        List<String> linList = new LinkedList<>();
        Main1.fillOfArrayList(arrList);
        Main2.fillOfLinkedList(linList);
        ListIterator<String> arrItr = arrList.listIterator();
        int index = arrList.size();


        while (arrItr.hasNext()){
            String element = arrItr.next();
            linList.add(index,element);
            index--;




        }
        printElements(linList);

    }
    public static void printElements(Collection<String> collection){
        Iterator<String> itr = collection.iterator();
        while (itr.hasNext()){
            System.out.println(itr.next());
        }
    }

}
