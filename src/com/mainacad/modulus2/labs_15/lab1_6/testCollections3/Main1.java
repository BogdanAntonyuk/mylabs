package com.mainacad.modulus2.labs_15.lab1_6.testCollections3;

import java.util.*;

public class Main1 {
    public static void fillOfArrayList(List<String> list){

        for (int i=0;i<10;i++){
            list.add("Number_"+i);
        }
    }

    public static void main(String[] args) {
        List <String> list = new ArrayList<>();
        fillOfArrayList(list);
        Iterator<String> itr = list.iterator();
        while (itr.hasNext()){
            System.out.println(itr.next());
        }
    }
}
