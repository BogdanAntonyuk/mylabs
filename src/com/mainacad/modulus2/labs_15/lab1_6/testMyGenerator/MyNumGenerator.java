package com.mainacad.modulus2.labs_15.lab1_6.testMyGenerator;

import java.util.*;

public class MyNumGenerator {
    int numOfElm;
    int maxNumb;
    MyNumGenerator(int numOfElem,int maxNumb){
        this.numOfElm = numOfElem;
        this.maxNumb = maxNumb;

    }
    public List<Integer> generate(){
        List<Integer> list = new ArrayList<>(numOfElm);
        Random rand = new Random();
        for (int i=0;i<numOfElm;i++){
            list.add(rand.nextInt(maxNumb));
        }
        return list;
    }


    public Set<Integer> generateDistinct(){
        Set<Integer> setList = new HashSet<>();
        Random rand = new Random();
        for (int i=0;i<numOfElm;i++){
            setList.add(rand.nextInt(maxNumb));
        }
        return setList;

    }
}

class Main4{
    public static void main(String[] args) {
        MyNumGenerator myNumGenerator =  new MyNumGenerator(5,2);
        System.out.println(myNumGenerator.generate());
        MyNumGenerator myNumGenerator1 = new MyNumGenerator(9,2);
        System.out.println("HashSet array: ");
        System.out.print(myNumGenerator1.generateDistinct());
    }
}
