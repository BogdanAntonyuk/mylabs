package com.mainacad.modulus2.labs_15.lab1_6.TestMap1;

import com.mainacad.modulus2.labs_15.lab1_6.testMyGenerator.MyNumGenerator;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MyTranslator {
    private HashMap<String, String> dictionary = new HashMap<>();
    void addNewWord(String en, String ukr){
        dictionary.put(en,ukr);
    }
    public String translate(String en){
        return dictionary.get(en);
    }

}
class Main0{
    public static void main(String[] args) {
        MyTranslator myTranslator = new MyTranslator();
        myTranslator.addNewWord("My","моя");
        myTranslator.addNewWord("cat","Киця");
        myTranslator.addNewWord("lovely","гарно");
        myTranslator.addNewWord("purrs", "муркоче");
        Scanner input = new Scanner(System.in);
        System.out.println("Add text to translate");
        String[] textToTranslate = input.nextLine().split(" ");
        for (String str:textToTranslate
             ) {
            System.out.print(myTranslator.translate(str)+" ");

        }
    }
}
