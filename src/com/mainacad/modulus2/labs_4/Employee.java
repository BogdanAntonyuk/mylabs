package com.mainacad.modulus2.labs_4;

import java.util.Scanner;

public class Employee {
    Scanner input = new Scanner(System.in);
    private String firstName;
    private String lastName;
    private String occupation;
    private int telephone;
    private static int numberOfEmployees;
    public Employee(){
        numberOfEmployees ++;
        System.out.print("Enter first name: ");
        firstName = input.nextLine();
        System.out.print("Enter last name: ");
        lastName = input.nextLine();
        System.out.print("Enter occupation?: ");
        occupation = input.next();
        System.out.print("Enter phone number?: ");
        telephone = input.nextInt();
        System.out.println("The number of Employees: "+numberOfEmployees);
    }
    String getFirstName(){
        return firstName;
    }
    void setFirstName(String firstName){
        this.firstName=firstName;
    }
    String getLastName(){
        return lastName;
    }
    void setLastName(String lastName){
        this.lastName=lastName;
    }
    String getOccupation(){
        return occupation;
    }
    void setOccupation(String occupation){
        this.occupation=occupation;
    }
    int getTelephone(){
        return telephone;
    }
    void setTelephone(int telephone){
        this.telephone=telephone;
    }

}
