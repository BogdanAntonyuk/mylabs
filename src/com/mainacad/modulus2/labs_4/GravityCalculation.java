package com.mainacad.modulus2.labs_4;

import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;

public class GravityCalculation {
    static final double acceleration = 9.81;
    static int initVelocity;
    static int initPosition;
    static double time = 10.00;
    static double distance;


    public static double calcDist(double time){
        distance = 0.5*acceleration*(time*time)+initVelocity*time+initPosition;
        return distance;


    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter time (in seconds): ");
        time = input.nextDouble();
        calcDist(time);
        System.out.println("The distance(in meters) is: "+ distance);
    }
}
