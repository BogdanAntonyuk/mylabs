package com.mainacad.modulus2.labs_4;

public class MyPyramid {
    public static void printPyramid(int h){
        switch (h){
            case 1: System.out.println("1"); break;
            case 2:
                System.out.println(" 1 \n121"); break;
            case 3:
                System.out.println("  1  \n 121 \n12321"); break;
            case 4:
                System.out.println("   1   \n  121  \n 12321 \n1234321");break;
            case 5:
                System.out.println("    1    \n   121   \n  12321  \n 1234321 \n123454321");break;
            case 6:
                System.out.println("     1     \n    121    \n   12321   \n  1234321  \n 123454321 \n12345654321");break;
            case 7:
                System.out.println("      1      \n     121     \n    12321    \n   1234321   \n  123454321  \n 12345654321 \n1234567654321");break;
            default:
                System.out.println("Please enter number in range from 1 to 7 (including)");


        }


    }

    public static void main(String[] args) {
        printPyramid(8);
    }
}
