package com.mainacad.modulus2.labs_3;

import java.util.Random;

public class MyWindow<color1> {
    double width;
    double height;
    int numberOfGlass;
    String color;
    boolean isOpen;
    String color1 = "";
    Random rand = new Random();
    public String toString(){
        return "MyWindow {Width = "+        String.format("%.2f",width)+" Height = "+String.format("%.2f",height)+" Number of glass = "+numberOfGlass+" Color = "+color+" open = "+isOpen+"}";
    }
    public void printFields(){
        System.out.println(toString());
    }
    String RandomColor(){
        int tColor = rand.nextInt(4);
        switch (tColor){
            case 1: color1 = "Red";
            break;
            case 2: color1 = "Green";
            break;
            case 3: color1 = "Yellow";
            break;
            default: color1 = "Black";
        }
        return color1;
    }

    MyWindow(){
        width = Math.random()*15;
        height = Math.random()*30;
        numberOfGlass = rand.nextInt(6);
        color = RandomColor();
        isOpen = true;

    }
    MyWindow(double width, double height){
        this.width = width;
        this.height = height;
        numberOfGlass = rand.nextInt(6);
        color = RandomColor();
        isOpen = true;

    }

}
