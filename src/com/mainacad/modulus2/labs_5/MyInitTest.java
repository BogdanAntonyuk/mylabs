package com.mainacad.modulus2.labs_5;

public class MyInitTest {
    {
        System.out.println("Non static init block 2");
    }
    static {
        System.out.println("Static init block 1");
    }
    MyInitTest(){
        System.out.println("Constructor1");
    }
    static {
        System.out.println("Static init block 2");
    }
    MyInitTest(int a){
        System.out.println("Constructor2");
    }
    {
        System.out.println("Non static init block 1");
    }

}
