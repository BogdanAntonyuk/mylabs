package com.mainacad.modulus2.labs_5;

public class InitTest {
    private int id;
    private static int nextId;
    static {
        nextId = (int)(100*Math.random());
    }
    {
        nextId++;
        id=nextId;
    }
    public int getId(){
        return id;
    }
}
class Main1 {
    public static void main(String[] args) {
        InitTest initTest1=new InitTest();
        System.out.println(initTest1.getId());
        InitTest initTest2=new InitTest();
        System.out.println(initTest2.getId());
        InitTest initTest3=new InitTest();
        System.out.println(initTest3.getId());
        InitTest initTest4=new InitTest();
        System.out.println(initTest4.getId());
        InitTest initTest5=new InitTest();
        System.out.println(initTest5.getId());

    }

}
