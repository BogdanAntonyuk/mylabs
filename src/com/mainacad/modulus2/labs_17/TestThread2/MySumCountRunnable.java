package com.mainacad.modulus2.labs_17.TestThread2;

import java.util.Random;

public class MySumCountRunnable implements Runnable {
    private int startIndex;
    private int stopIndex;
    private int[] arr;


    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getStopIndex() {
        return stopIndex;
    }

    public void setStopIndex(int stopIndex) {
        this.stopIndex = stopIndex;
    }


    public void setArr(int[] arr) {
        this.arr = arr;
    }

    private long resultSum;

    public long getResultSum() {
        return resultSum;
    }

    @Override
    public void run() {
        for(int i = startIndex;i<stopIndex;i++){
            resultSum += arr[i];
        }
        System.out.println(Thread.currentThread().getName()+ " has finished counting. "+"The result is: "+resultSum);
    }
}
class Main{
    public static void main(String[] args) {
        int[] myArray = new int[1000];
        Random rand = new Random();
        for (int i = 0;i<myArray.length;i++){
            myArray[i] = rand.nextInt(10);
        }
        MySumCountRunnable mySumCountRunnable1 = new MySumCountRunnable();
        MySumCountRunnable mySumCountRunnable2 = new MySumCountRunnable();
        Thread thr1 = new Thread(mySumCountRunnable1);
        Thread thr2 = new Thread(mySumCountRunnable2);
        mySumCountRunnable1.setStartIndex(0);
        mySumCountRunnable1.setStopIndex(myArray.length/2);
        mySumCountRunnable1.setArr(myArray);
        thr1.start();
        mySumCountRunnable2.setStartIndex(myArray.length/2+1);
        mySumCountRunnable2.setStopIndex(myArray.length);
        mySumCountRunnable2.setArr(myArray);
        thr2.start();
        try {
            thr1.join();
            thr2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("The sum of the whole array is: " +
                (mySumCountRunnable1.getResultSum()+mySumCountRunnable2.getResultSum()));


    }
}
