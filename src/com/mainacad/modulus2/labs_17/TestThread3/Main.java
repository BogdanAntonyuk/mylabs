package com.mainacad.modulus2.labs_17.TestThread3;


class Storage{
    private boolean flag;

    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}


class Counter implements Runnable{
    Storage stor;
    public Counter(Storage stor){
        this.stor = stor;
    }
    @Override
    public void run() {

        synchronized (stor) {
            for (int i = 0; i < 1000; i++) {
               while (stor.isFlag()){
                   try {
                       stor.wait();
                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }
               }
               stor.setValue(i);
               stor.setFlag(true);
               stor.notifyAll();
            }


        }

    }
}


class Printer implements Runnable{

    Storage stor;
public Printer(Storage stor){
   this.stor = stor;
}
    @Override
    public void run() {
//        System.out.println("Waiting for data #"+Thread.currentThread().getName()+"...");

        synchronized (stor) {
            try {
                while (!stor.isFlag()) {
                    stor.wait();
                }
            } catch (InterruptedException e) {
            }
            System.out.println(stor.getValue());
            stor.setFlag(false);
            stor.notifyAll();
        }
    }
}


public class Main {
    public static void main(String[] args) {
        Storage storage = new Storage();
        Counter counter = new Counter(storage);
        Printer printer = new Printer(storage);
        Thread thr1 = new Thread(counter);
        Thread thr2 = new Thread(printer);
        thr1.start();
        thr2.start();

    }
}
