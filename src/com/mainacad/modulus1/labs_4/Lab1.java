package com.mainacad.modulus1.labs_4;

public class Lab1 {
    public static void main(String[] args) {


        byte z = 12;
        System.out.println("Byte = " + z); //exports byte
        short x = 128;
        System.out.println("Short = " + x);
        int c = 876;
        System.out.println("Integer = " + c);
        long v = 1245L;
        System.out.println("Long = " + v);
        float b = 12.35F;
        System.out.println("Float = " + b);
        double n = 345.675;
        System.out.println("Double = " + n);
        char m = 'm';
        System.out.println("Char = " + m);
        boolean a = false;
        System.out.println("Boolean = " + a);
    }




}
