package com.mainacad.modulus1.labs_4;

public class Lab4 {
    public static void main(String[] args) {
        int a = 0;
        int b = a++;
        System.out.println("b = "+b);
        System.out.println("a = "+a);
        b = ++a;
        System.out.println("b = "+b);
        System.out.println("a = "+a);
        int c = ++a;
        System.out.println("c = "+c);
        System.out.println("a = "+a);
        c = a++;
        System.out.println("c = "+c);
        System.out.println("a = "+a);
        c=a;
        System.out.println("c = "+c);
        System.out.println("a = "+a);

    }
}
