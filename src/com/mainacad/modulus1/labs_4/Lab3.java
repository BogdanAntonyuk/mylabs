package com.mainacad.modulus1.labs_4;

public class Lab3 {
    public static void main(String[] args) {
        boolean a = true;
        boolean b = false;
        boolean c = a&&a;
        boolean d = b||b;
        System.out.println("The only way for \"true\" result in \"and\" logic are both variables must be \"true\". a&&a = " + c);
        System.out.println("The only way for \"false\" result in \"or\" logic are both variables must be \"false\". b||b = " + d);
    }
}
