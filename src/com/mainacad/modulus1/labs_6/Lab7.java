package com.mainacad.modulus1.labs_6;



import java.util.Random;
import java.util.Scanner;

public class Lab7 {
    public static void main(String[] args) {
        int [][] arr1 = new int[5][5];                         //adjusting random numbers to array
        Random rand = new Random();
        for (int i=0; i<arr1.length; i++){
            for (int j=0; j<arr1[i].length; j++){
                arr1[i][j] = rand.nextInt(6);
            }
        }
        System.out.println("There is The matrix!");                //Printing out Matrix
        for (int i = 0; i<arr1.length;i++){
            for (int j=0;j<arr1[i].length;j++){
                if (arr1[i][j]<10||arr1[i][j]<-10){
                    System.out.print(" "+arr1[i][j]+" ");
                }else System.out.print(arr1[i][j]+" ");
            }
            System.out.println();
        }
        System.out.print("What number do you want to search in The matrix?: ");    //Input number via Scanner and searching
        Scanner input = new Scanner(System.in);
        int sValue = input.nextInt();
        for (int i=0; i<arr1.length; i++){
            System.out.print("Line "+ (i+1) +": [");
            for (int j=0; j<arr1[i].length;j++){
                if (arr1[i][j]==sValue){
                    System.out.print(j);
                    System.out.print(",");

                }
            }
            System.out.print("]");
            System.out.println();
        }
    }
}
