package com.mainacad.modulus1.labs_6;

public class Lab5 {
    public static void main(String[] args) {
        int[][] originalM = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
        System.out.println("Original Matrix: ");
        displayMatrix(originalM);
        System.out.println();

        int [][] transpodedMatrix = transposeMatrix(originalM);
        System.out.println("Transposed Matrix: ");

        displayMatrix(transpodedMatrix);
    }

    public static int[][] transposeMatrix(int[][] matrix){
        int m = matrix.length;
        int n = matrix[0].length;

        int[][] transposedMatrix = new int[n][m];

        for(int x = 0; x < n; x++) {
            for(int y = 0; y < m; y++) {
                transposedMatrix[x][y] = matrix[y][x];
            }
        }

        return transposedMatrix;
    }
    public static void displayMatrix(int[][] matrix){
        for (int i = 0; i<matrix.length;i++){
            for (int j=0;j<matrix[i].length;j++){
                if (matrix[i][j]<10||matrix[i][j]<-10){
                    System.out.print(" "+matrix[i][j]+" ");
                }else System.out.print(matrix[i][j]+" ");
            }
            System.out.println();
        }
    }
}
