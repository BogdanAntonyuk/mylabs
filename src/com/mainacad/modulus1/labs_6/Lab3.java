package com.mainacad.modulus1.labs_6;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Lab3 {
    public static void main(String[] args) {
        int[][] m4x4 = {
                {1,5,9,13},
                {2,6,10,14},
                {3,7,11,15},
                {4,8,12,16}
                };
        for (int i = 0; i < m4x4.length; i++) {         //this equals to the row in our matrix.
            for (int j = 0; j < m4x4[i].length; j++) {   //this equals to the column in each row.
                System.out.print(m4x4[i][j] + " ");
            }
            System.out.println();
        }

    }
}
