package com.mainacad.modulus1.labs_5;
import java.util.Scanner;

public class Lab6 {
    public static void main(String[] args) {
        int number = new Scanner(System.in).nextInt();
        int sum = 0;
        if (number > -10 && number < 10) {
            sum = number * number;
        } else {
            for (int i = 0; ; i++) {
                if (number > -10 && number < 10) {
                    sum = sum + (number * number);
                    break;
                } else {
                    sum = sum + ((number % 10) * (number % 10));
                    number = number / 10;
                }
            }
        }
        System.out.println(sum);
    }
}
