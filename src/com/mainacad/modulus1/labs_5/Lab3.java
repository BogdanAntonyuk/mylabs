package com.mainacad.modulus1.labs_5;

public class Lab3 {
    public static void main(String[] args) {
        System.out.println("* | 1  2  3  4  5  6  7  8  9");
        System.out.println("----------------------------------");
        for(int i=1;i<=9;i++){
            System.out.print(i + " " + "|");
            for(int j=1;j<=9;j++){
                int m = j*i;
                if (m<10) {
                    System.out.print(" " + m + " ");
                }else {
                    System.out.print(" " +m);
                }
            }
            System.out.println();
        }

    }
}
