package com.mainacad.modulus3.labs1.lab2;

import java.io.*;

public class PrintFile {
    public static void main(String[] args) {
        try(BufferedReader br = new BufferedReader(new FileReader("src/files/File.txt"))){

            String str;
            while ((str = br.readLine()) != null){
                System.out.println(str);
            }
        }catch (IOException e){
            e.printStackTrace();

        }




    }
}
class MyFileCopy{
    public static void main(String[] args) {
        try(InputStream inputStream = new FileInputStream("src/files/File.txt");
            OutputStream outputStream = new FileOutputStream("src/files/FileCopy.txt"))
        {
            int c = 0;
            while ((c = inputStream.read()) != -1) {
                outputStream.write(c);
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
