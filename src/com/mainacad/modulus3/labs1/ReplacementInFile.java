package com.mainacad.modulus3.labs1;

import java.io.*;
import java.util.Scanner;

public class ReplacementInFile {
    public static void main(String[] args) {
        try{
            BufferedReader fileReader = new BufferedReader(new FileReader("src/files/ForReplace.java"));
            StringBuffer stringBuffer = new StringBuffer();
            String line;

            while ((line = fileReader.readLine()) != null){
                stringBuffer.append(line + "\n");
            }
            fileReader.close();

            String inputData = stringBuffer.toString();
            System.out.println(inputData);
            String outData = inputData.replaceAll("public","private");

            //BufferedWriter fileWriter = new BufferedWriter(new FileWriter("src/files/ForReplace.java"));
            FileWriter fileWriter = new FileWriter("src/files/ForReplace.java");
            System.out.println(outData);

            fileWriter.write(outData,0,outData.length());
            fileWriter.close();

        }catch (IOException e){
            e.printStackTrace();
        }


    }
}
