package com.mainacad.modulus3;


import java.sql.*;



class JdbcTest {
    public static void main(String[] args) {
        final String db_url="jdbc:mysql://localhost:3306/test";
        final String db_user="root";
        final String db_password="password";
        final String db_driver="com.mysql.cj.jdbc.Driver";
        try {
            Class.forName(db_driver); // initialize driver
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try(Connection conn = DriverManager.getConnection(db_url, db_user, db_password)) {
            Statement st = conn.createStatement();
            st.execute("select * from my_first_table"); // may return more than one recordset
            st.executeQuery("select * from my_first_table"); // return only single recordset
            ResultSet rs = st.getResultSet();
            while (rs.next()) {
                System.out.println(rs.getString(2));
            }
            System.out.println("Prepared Statement");
            PreparedStatement ps = conn.prepareStatement("select * from my_first_table where id = ?");
            ps.setInt(1, 14);
            ps.executeQuery();
            rs = ps.getResultSet();
            while(rs.next()) {
                System.out.println(rs.getString(2));
            }
            System.out.println("Meta Data");
            ResultSetMetaData rsm = ps.getMetaData();
            for (int i = 1; i <= rsm.getColumnCount(); i++) {
                System.out.print(rsm.getColumnName(i) + "\t\t");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
