package com.mainacad.modulus3.labs8.DemoSoket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

public class ThreadClient implements Runnable {
    private Socket socket;
    private List<Student> users;

    public ThreadClient(Socket socket, List<Student> users) {
        this.socket = socket;
        this.users = users;
    }

    @Override
    public void run() {
        try {

            ObjectOutputStream os =
                    new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream oi =
                    new ObjectInputStream(socket.getInputStream());
            Student std = (Student)oi.readObject();
            if (users.contains(std)) {
                os.writeObject("Student " + std.getName() + " - access opened");
            } else {
                os.writeObject("Student " + std.getName() + " - access denied");
            }
        } catch (IOException | ClassNotFoundException exp) {
            exp.printStackTrace();
        }
    }
}
