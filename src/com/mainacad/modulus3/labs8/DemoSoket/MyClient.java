package com.mainacad.modulus3.labs8.DemoSoket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class MyClient implements Runnable {
    private Student student;
    MyClient(Student student){
        this.student = student;

    }
    @Override
    public void run() {
        try(Socket socket = new Socket("localhost", 8899)) {
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            ObjectOutputStream ous = new ObjectOutputStream(socket.getOutputStream());

            ous.writeObject(student);
            ous.flush();
            System.out.println("server response: "+ois.readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
