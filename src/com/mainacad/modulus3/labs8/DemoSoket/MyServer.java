package com.mainacad.modulus3.labs8.DemoSoket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

public class MyServer implements Runnable{
    private List<Student> users;



    @Override
    public void run() {
        try(ServerSocket listener = new ServerSocket(8899)) {
            System.out.println("Server is waiting...");

            Socket socket = listener.accept();
            System.out.println("Server connected!");
//            ObjectOutputStream ous = new ObjectOutputStream(socket.getOutputStream());
//            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            init();

            System.out.println("Server connected with " + socket.getInetAddress().getHostName());
            new Thread(new ThreadClient(socket, users)).start();


//            Student student = (Student) ois.readObject();
//            users.add(student);
//            System.out.println(student);
//            ous.writeObject(student.getName()+" receiver");
//            ous.flush();





//            ois.close();
//            ous.close();
//            System.out.println("The Job is done");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void init() {
        users.add(new Student("Igor", "C#"));
        users.add(new Student("Alex", "Java SE"));
        users.add(new Student("Helena", "JavaScript"));
        users.add(new Student("Ivan", "C#"));
        users.add(new Student("Natali", "JavaScript"));
        users.add(new Student("Oleg", "C#"));
        users.add(new Student("Dina", "Java SE"));
    }

}
