package com.mainacad.modulus3.labs8.DemoSoket;

import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        new Thread(new MyServer()).start();
        new Thread(new MyClient(new Student("Alex", "Java SE"))).start();
        new Thread(new MyClient(new Student("Oleg", "Java SE"))).start();
        new Thread(new MyClient(new Student("Dina", "Java SE"))).start();
        new Thread(new MyClient(new Student("Ivan", "C#"))).start();
        new Thread(new MyClient(new Student("Michael", "C#"))).start();
    }
}
