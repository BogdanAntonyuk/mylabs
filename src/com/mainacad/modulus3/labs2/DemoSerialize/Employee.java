package com.mainacad.modulus3.labs2.DemoSerialize;

import java.io.*;

public class Employee implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private String address;
    private int SSN;
    private int number;

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", SSN=" + SSN +
                ", number=" + number +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getSSN() {
        return SSN;
    }

    public void setSSN(int SSN) {
        this.SSN = SSN;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
class Main{
    public static void main(String[] args) {
        Employee employee = new Employee();
        employee.setName("bob");
        employee.setAddress("stre 1");
        employee.setSSN(767);
        employee.setNumber(88573930);
        try(ObjectOutputStream ooe = new ObjectOutputStream(new FileOutputStream("src/files/EmployeeInfo.ser"))){
            ooe.writeObject(employee);
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println("serialized");


    }
}
class DeSerialize{
    public static void main(String[] args) {
        Employee e2;
        try(ObjectInputStream oie = new ObjectInputStream(new FileInputStream("src/files/EmployeeInfo.ser"))) {
            e2 = (Employee) oie.readObject();
            System.out.println(e2.toString());

        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }
}
