package com.mainacad.modulus3.labs9;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.logging.Handler;

public class DemoProxy1 {
    public static void main(String[] args) {
//        Calculatelmpl calculate = new Calculatelmpl();
//        CalculateProxy calculateProxy = new CalculateProxy(calculate);
        Calculate calc = (Calculate) CalculateProxy.newInst(new Calculatelmpl());
        calc.division(9,2);
        calc.multiplication(2,2);
    }



}
interface Calculate{
    double multiplication(double a,double b);
    double division(double a,double b);
}
class Calculatelmpl implements Calculate{

    @Override
    public double multiplication(double a, double b) {
        double result = a*b;
        return result;
    }

    @Override
    public double division(double a, double b) {
        double result = a/b;
        return result;
    }
}
class CalculateProxy implements InvocationHandler {
    private Object objCalc;
    CalculateProxy(Object objCalc){
        this.objCalc = objCalc;
    }
    public static Object newInst(Object obj){
        return Proxy.newProxyInstance(obj.getClass().getClassLoader(),obj.getClass().getInterfaces(),new CalculateProxy(obj));
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        StringBuffer sb = new StringBuffer();
        sb.append(method.getName());
        sb.append("(");
        for (int i =0;args!=null&&i<args.length;i++){
            if (i !=0)
                sb.append(", ");
            sb.append(args[i]);
        }
        sb.append(")");

        Object result = method.invoke(objCalc, args);
        if(result!=null){
            sb.append(" --> ");
            sb.append(result);
        }
        System.out.println(sb);
//        System.out.println(method.getName()+ "("+ args +") --> "+ result.toString());

        return result;
    }
}

