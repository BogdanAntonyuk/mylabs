package com.mainacad.modulus3.labs10;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class Bitwise {
}
interface Calculate{
    double multiplication(double a,double b);
    double division(double a,double b);
}
class Calculatelmpl implements Calculate {

    @Override
    public double multiplication(double a, double b) {
        double result = a*b;
        return result;
    }

    @Override
    public double division(double a, double b) {
        double result = a/b;
        return result;
    }
}
interface CalculateBitwise{
    int andBitwise(int a,int b);
    int orBitwise(int a,int b);

}
class CalculateBitwiseImp implements CalculateBitwise{

    @Override
    public int andBitwise(int a, int b) {
        return a&b;
    }

    @Override
    public int orBitwise(int a, int b) {
        return a|b;
    }
}
class CalculateProxy implements InvocationHandler{
    private Class Interfaces[];
    private Object Deligators[];
    private CalculateProxy(Class[] Interfaces, Object[] Deligators){
        this.Interfaces = Interfaces;
        this.Deligators= Deligators;
    }



    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return null;
    }
}