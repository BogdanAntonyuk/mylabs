package com.mainacad.modulus3.labs6.TestAnnotation2;

import java.util.Collections;

public class Main {
    public static void main(String[] args) throws NoSuchMethodException {
        Action action = new Action();
        User user1 = new User("Ivanov");
        user1.setPermissions(Collections.singletonList(PermissionAction.USER_READ));
        action.read(user1);
        action.write(user1);
        User user2 = new User("Petrov");
        user2.setPermissions(Collections.singletonList(PermissionAction.USER_CHANGE));
        user2.setPermissions(Collections.singletonList(PermissionAction.USER_READ));
        action.read(user2);
        action.write(user2);


    }
}
