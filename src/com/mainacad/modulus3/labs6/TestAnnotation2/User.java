package com.mainacad.modulus3.labs6.TestAnnotation2;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String name;
    List<PermissionAction> permissions = new ArrayList<>();
    public User(String name){
        this.name = name;
    }

    public List<PermissionAction> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionAction> permissions) {
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        return "User's name= " + name;
    }
}
