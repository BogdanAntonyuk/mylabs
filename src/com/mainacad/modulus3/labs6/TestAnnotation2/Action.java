package com.mainacad.modulus3.labs6.TestAnnotation2;

import java.io.*;
import java.lang.reflect.Method;

public class Action {
    File file;
    public Action(){
        this.file = new File("src/files/data.txt");
    }
    private String readFile(){
        String text = "";
        String line = null;
        try(BufferedReader reader = new BufferedReader(new FileReader(file))){
            while((line = reader.readLine())!=null){
                text = text + line + "\n";
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return text;
    }
    private void writeFile() {
        try(RandomAccessFile writer = new RandomAccessFile(file,"rw")){
            writer.seek(file.length());
            writer.write(Integer.parseInt("Test text"));

        }catch (IOException e){
            e.printStackTrace();
        }


    }
    @MyPermission(PermissionAction.USER_READ)
    public String read(User user) throws NoSuchMethodException {
        String text = null;
            Method mm = this.getClass().getMethod("read",User.class);
            MyPermission permissionAction = mm.getAnnotation(MyPermission.class);

        if(user!=null && (user.getPermissions().contains(PermissionAction.USER_READ))){
            text = readFile();
            System.out.println("User: " + user+" Read is successful");
        }else System.out.println("Read operation is denied");

        return text;

    }
    @MyPermission(PermissionAction.USER_CHANGE)
    public void write(User user) throws NoSuchMethodException {
        Method mm = this.getClass().getMethod("write", User.class);
        MyPermission permissionAction = mm.getAnnotation(MyPermission.class);
        if(user!=null &&(user.getPermissions().contains(PermissionAction.USER_CHANGE))){
            writeFile();

        }else System.out.println("Write operation is denied");
    }


}
